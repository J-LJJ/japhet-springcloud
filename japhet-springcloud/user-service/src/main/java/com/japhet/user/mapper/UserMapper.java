package com.japhet.user.mapper;

import com.japhet.user.pojo.User;
import tk.mybatis.mapper.common.Mapper;

public interface UserMapper extends Mapper<User> {

}
