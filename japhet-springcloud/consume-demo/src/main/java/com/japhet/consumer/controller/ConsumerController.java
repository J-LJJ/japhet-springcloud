package com.japhet.consumer.controller;

import com.japhet.consumer.pojo.User;
import com.netflix.hystrix.contrib.javanica.annotation.DefaultProperties;
import com.netflix.hystrix.contrib.javanica.annotation.HystrixCommand;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cloud.client.ServiceInstance;
import org.springframework.cloud.client.discovery.DiscoveryClient;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.client.RestTemplate;

import java.util.List;

@RestController
@RequestMapping("/consumer")
@Slf4j
@DefaultProperties(defaultFallback = "defaultFallback")
public class ConsumerController {

    @Autowired
    private RestTemplate restTemplate;

    @Autowired
    private DiscoveryClient discoveryClient;

    @GetMapping("/{id}")
//    @HystrixCommand(fallbackMethod = "queryByIdFallback")
    @HystrixCommand
    public String queryById(@PathVariable Long id){
//        if(id==1){
//            throw new RuntimeException("太忙了");
//        }
        /*String url = "http://localhost:9091/user/"+id;

        //获取eureka中的user-service的实例
        List<ServiceInstance> instances = discoveryClient.getInstances("user-service");
        ServiceInstance serviceInstance = instances.get(0);
        url = "http://"+serviceInstance.getHost()+":"+serviceInstance.getPort()+"/user/"+id;*/

        //再执行RestTemplate发送服务地址请求的时候，使用负载均衡拦截器拦截，根据服务名获取服务器地址列表，使用ribbon负载均衡算法从服务地址列表中选择一个服务地址，访问该地址获取服务数据
        //启动多个user-service实例（9091,9092）
        String url = "http://user-service/user/"+id;//使用ribbon负载均衡

        return restTemplate.getForObject(url,String.class);
    }


    public String queryByIdFallback(Long id){
        log.error("查询用户失败，id:{}",id);
        return "网络繁忙，请稍后重试";
    }
    public String defaultFallback(){
        return "默认提示：网络繁忙，请稍后重试";
    }

}
