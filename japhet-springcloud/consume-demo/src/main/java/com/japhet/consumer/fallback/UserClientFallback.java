package com.japhet.consumer.fallback;

import com.japhet.consumer.client.UserClient;
import com.japhet.consumer.pojo.User;
import org.springframework.stereotype.Component;

@Component
public class UserClientFallback implements UserClient {
    @Override
    public User queryById(Long id) {
        User user = new User();
        user.setId(id);
        user.setUserName("用户异常");
        return user;
    }
}
