package com.japhet.consumer.client;

import com.japhet.consumer.config.FeignConfig;
import com.japhet.consumer.fallback.UserClientFallback;
import com.japhet.consumer.pojo.User;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;

//声明当前类是一个Feign客户端，指定服务名为user-service
@FeignClient(value = "user-service",fallback = UserClientFallback.class,configuration = FeignConfig.class)
public interface UserClient {

    //http://user-service/user/1
    @GetMapping("/user/{id}")
    User queryById(@PathVariable Long id);

}
